
#Script corrects MMS observations based on input from the editor/editorAssessment
 
import requests
from requests.auth import HTTPBasicAuth
import pandas as pd
import json
import os
import datetime



db_fields = ['eventID', 'eventDate', 'decimalLatitude', 'decimalLongitude', 'locality',
'locationRemarks','scientificName','adultM','adultF', 'adult', 'subAdult',
'polarBearCondition','polarBearDen','cubCalfPup', 'bearCubs', 'deadAlive',
'unindentified', 'individualCount','habitat', 'eventRemarks', 'excelfile',
'contactInfo', 'organisation', 'platform','platformComment', 'startDate',
'endDate', 'recordedBy', 'recordedByName']


#This file reads a user and password from a json file
file = open("config.json", "r")
res = file.read()
json_res = json.loads(res)
user = json_res['user']
password = json_res['password']

#Read through all files in the directory
with os.scandir('./excel') as entries:
    for excel in entries:
        print(excel.name)

        #data = pd.read_excel(r'./excel/Sei.xlsx',sheet_name='Sheet1')
        data = pd.read_excel(r'./excel/'+excel.name,sheet_name='Sheet1')

        #Find index for last row with data in excel schema
        stop = data.index.stop

        #Iterate through quality - if good, skip. 828
        for i in range(0,stop,1):

           assessment = data.values[i][29]

           #print (data.values[i])
           #Get ID from excelfile
           id = data.values[i][0]
           print (id)


           #Search and get the row from the database
           down_path = 'https://v2-api.npolar.no/sighting/marine-mammal/observation/'+str(id).replace(" ","")
           r = requests.get(down_path, auth = HTTPBasicAuth(user,password))
           res_str = r.text
           #print(res_str)

           res = json.loads(res_str)
           if not pd.isna(data.values[i][29]): #or data.values[i][29] != ' ':
              res['dynamicProperties']['editorAssessment'] = (data.values[i][29])
           #test_30 = pd.isna(data.values[i][30])
           if not pd.isna(data.values[i][30]): # or data.values[i][30] != ' ' :
               res['dynamicProperties']['editorQualityTest'] = (data.values[i][30])
           if not pd.isna(data.values[i][31]):    #or data.values[i][31] != ' ' :
               res['dynamicProperties']['editorComment'] = (data.values[i][31])
           res['dynamicProperties']['editorDate'] = '2020-10-20T12:00:00Z'
           res['dynamicProperties']['editorName'] = 'Olof Bengtsson'

           #Iterate through to find ¤
           for j in range(29):
              if (str(data.values[i][j]).find("¤")) > -1:

                 update_val2 = data.values[i][j]
                 update_val = update_val2[1:]

                 #Separate dynamicProperties from Darwin Core variables
                 if  db_fields[j] == 'eventDate':
                     res['eventDate'] = update_val.replace("¤", "").strip() + "T00:00:00Z" #datetime.datetime.strptime(update_val, '%Y-%m-%d').isoformat()
                 elif  db_fields[j] ==  'decimalLatitude':
                     res['decimalLatitude'] = float(update_val)
                 elif  db_fields[j] == 'decimalLongitude':
                     res['decimalLongitude'] = float(update_val)
                 elif  db_fields[j] == 'locality':
                     res['locality'] = update_val.replace("¤", "").strip()
                 elif  db_fields[j] == 'locationRemarks':
                     res['locationRemarks'] = update_val
                 elif  db_fields[j] == 'scientificName':
                     res['scientificName'] = update_val.replace("¤", "").strip().capitalize()
                 elif  db_fields[j] == 'adultM':
                     res['dynamicProperties']['adultM'] = int(update_val)
                 elif  db_fields[j] == 'adultF':
                     res['dynamicProperties']['adultF'] = int(update_val)
                 elif  db_fields[j] == 'adult':
                     res['dynamicProperties']['adult'] = int(update_val)
                 elif  db_fields[j] == 'subAdult':
                     res['dynamicProperties']['subAdult'] = int(update_val)
                 elif  db_fields[j] == 'polarBearCondition':
                     res['dynamicProperties']['polarBearCondition'] = update_val
                 elif  db_fields[j] == 'polarBearDen':
                     res['dynamicProperties']['polarBearDen'] = update_val
                 elif  db_fields[j] == 'cubCalfPup':
                     res['dynamicProperties']['cubCalfPup'] = int(update_val)
                 elif  db_fields[j] == 'bearCubs':
                     res['dynamicProperties']['bearCubs'] = update_val
                 elif  db_fields[j] == 'deadAlive':
                     res['dynamicProperties']['deadAlive'] = update_val
                 elif  db_fields[j] == 'unindentified':
                     res['dynamicProperties']['unindentified'] = int(update_val)
                 elif  db_fields[j] == 'individualCount':
                     res['individualCount'] = int(update_val)
                 elif  db_fields[j] == 'habitat':
                     res['habitat'] = update_val
                 elif  db_fields[j] == 'eventRemarks':
                     res['eventRemarks'] = update_val
                 elif  db_fields[j] == 'excelfile':
                     res['dynamicProperties']['sourcePath'] = update_val
                 elif  db_fields[j] == 'contactInfo':
                     res['dynamicProperties']['contactInfo'] = update_val
                 elif  db_fields[j] == 'organisation':
                     res['dynamicProperties']['organisation'] = update_val
                 elif  db_fields[j] == 'platform':
                     res['dynamicProperties']['platform'] = update_val
                 elif  db_fields[j] == 'platformComment':
                     res['dynamicProperties']['platformComment'] = update_val
                 elif  db_fields[j] == 'startDate':
                     res['dynamicProperties']['startDate'] = update_val
                 elif  db_fields[j] == 'endDate':
                     res['dynamicProperties']['endDate'] = update_val
                 elif  db_fields[j] == 'recordedBy':
                     res['dynamicProperties']['recordedBy'] = update_val
                 elif  db_fields[j] == 'recordedByName':
                     res['dynamicProperties']['recordedByName'] = update_val


           #Convert json back to string before transmission
           res_str2 = json.dumps(res)
           print(res_str2)


           up_path = 'https://v2-api.npolar.no/sighting/marine-mammal/observation/'+str(id).replace(" ","")
           resp = requests.put(up_path, data=res_str2, auth = HTTPBasicAuth(user,password),headers={'Content-type': 'application/json'})


           print(resp.headers)
           print(resp.status_code)
           print(resp.text)
